const FileSystem =require("fs");
const filewrite = (content,filename) => {
    FileSystem.writeFile(`../output/${filename}`, JSON.stringify(content, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log(`${filename} File has been created`);
    });
};







    module.exports = {
        matchPerYear:  matchPerYear = (matches) => {
            var matchesPerYear = {};
            for (let i=0; i < matches.length; i++) {
                let season =matches[i]["season"];
            
                if (matchesPerYear[season] == undefined) {
                    matchesPerYear[season] = 1;
                    continue;
                }
                matchesPerYear[season] = matchesPerYear[season] + 1;        
               
            }
            //console.log(mat);
            filewrite(matchesPerYear,"matchesPerYear.json");
            },




            winerPerYear: winerPerYear = (matches) => {
            var winnerPerYear = {};
            
            for (let i=0; i < matches.length; i++) {
                let season =matches[i]["season"];
                let winner =matches[i]["winner"];
                if (winnerPerYear[season] == undefined) {
                    winnerPerYear[season] = {};                  
                    winnerPerYear[season][winner] = 1;
                }
                else {
                    if (winnerPerYear[season][winner] == undefined) {
                        
                        winnerPerYear[season][winner] = 1;
                    }
                    else {
                        winnerPerYear[season][winner] += 1;        
                    }      
                } 
            }
            //console.log(winner);
            filewrite(winnerPerYear,"winnerPerYear.json");
            },






            extraRunInYear: extraRunInYear = (matches, deliveries) => {
                var matchid = [];
                var extraPerTeam = {};
                for (let i=0; i < matches.length; i++) {
                    let id =matches[i]["id"];
                    if (matches[i]["season"] == 2016) {
                        matchid.push(id); 
                    }       
                }

                for (let i=0; i< matchid.length; i++) {
                    for(let j=0; j<deliveries.length; j++) {
                        let bowling_team =deliveries[j]["bowling_team"];
                        let extra_runs =deliveries[j]["extra_runs"];
                        if(matchid[i] == deliveries[j]["match_id"]) {
                            if (extraPerTeam[bowling_team] == undefined) {
                                extraPerTeam[bowling_team] = parseInt(extra_runs);
                                continue;
                            }
                            extraPerTeam[bowling_team] += parseInt(extra_runs);
                        }
                    }
                }
                
               // console.log(mat);
                //console.log(extra);
                filewrite(extraPerTeam,"extraPerTeam.json");
            },






            avgInYear: avgInYear = (matches, deliveries) => {
                let matchid = [];
                for (let i=0;i < matches.length; i++) {
                    if(matches[i]['season'] == 2015) {
                        matchid.push(matches[i]['id']);
                    }
                }
                let bowler ={};
                let econimicalRate = [];
                let bowlerName =[];
                for(let i=0; i< matchid.length; i++) {
                    for (let j=0; j < deliveries.length; j++)
                    {
                        let bowlerFromobject = deliveries[j]['bowler'];
                        let runs = deliveries[j]['total_runs'];
                        if(deliveries[j]['match_id'] == matchid[i])
                        {
                            
                            if(bowler[bowlerFromobject]) {
                                bowler[bowlerFromobject]["runs"] += parseInt(runs);
                                bowler[bowlerFromobject]["boll"] += 1;
                
                              
                            } else {
                                bowler[bowlerFromobject] = {};
                                bowler[bowlerFromobject]["runs"] = parseInt(runs);
                                bowler[bowlerFromobject]["boll"] = 1;
                                bowlerName.push(bowlerFromobject);
                            }
                         
                
                    }
                }
                }
                for (let i=0; i <bowlerName.length;i++) {
                    let name =bowlerName[i];
                    bowler[name]=bowler[name]["runs"]/bowler[name]["boll"];
                    econimicalRate.push(bowler[name]);
                }
                econimicalRate.sort();
               
                let econimicalbowlerwithName={};
                for (let i=0; i < 10;i++) {
                    
                    for (let j=0; j <bowlerName.length;j++) {
                        let name =bowlerName[j];
                        if(bowler[name] == econimicalRate[i]) {
                            econimicalbowlerwithName[name]=bowler[name];
                        }
    
                    }
                  
                }
    
                
                //console.log(econimicalbowlerwithName);
                filewrite(econimicalbowlerwithName,"top10EconomicalBowler.json");
            }
      
      };
      